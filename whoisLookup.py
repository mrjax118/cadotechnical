import whois
from flask import Flask, request, render_template

# Initialises flask app and defines the GET method for use
app = Flask(__name__)
@app.route('/',methods = ['GET'])

def lookup():
    lookup = request.args.get('lookup')
    res = whois.whois(lookup)

    # Changes datetime obj 'updated_date' into readable format
    dtFormat = '%Y-%m-%d %H:%M:%S'
    for x in range(len(res.updated_date)):
        res.updated_date[x] = res.updated_date[x].strftime(dtFormat)

    # A list of keys to extract from the 'whois' data
    lst = ["domain_name", 
            "registrar", 
            "whois_server", 
            "referral_url", 
            "updated_date"]

    # Passes 'toDisplay' to 'index.html in a <pre> to maintain line breaks
    return render_template('lookupTemplate.html', toDisplay = res, lst = lst, valid = True)

if __name__ == '__main__':
    app.run('localhost', 8080)