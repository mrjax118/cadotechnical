# Cado Technical Solution

This is a simple web app that takes a user-given domain or ip address and returns Whois information. The application uses Flask to manage a 'GET' method and pass data to be displayed on a HTML page using Jinja.


## Dependancies
flask 2.0.2  
python-whois 0.7.13  
dir 'templates' with 'lookupTemplate.html'
